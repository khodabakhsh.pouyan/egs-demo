package com.egs.demo.service;

import com.egs.demo.model.dto.request.CreateCommentRequest;
import com.egs.demo.model.dto.response.CommentResponse;

import java.util.List;

public interface CommentService {

    CommentResponse addComment(CreateCommentRequest createCommentRequest);

    List<CommentResponse> getProductCommentsByproductId(long productId);
}
