package com.egs.demo.service;

import com.egs.demo.model.dto.request.CreateProductRequest;
import com.egs.demo.model.dto.request.RatingRequest;
import com.egs.demo.model.dto.response.ProductResponse;
import com.egs.demo.model.entity.Product;
import org.springframework.data.jpa.domain.Specification;

import java.util.List;

public interface ProductService {

    List<ProductResponse> findAll(Specification<Product> specification);

    int rateProduct(RatingRequest ratingRequest);

    void delete(Product product);

    void deleteById(long productId);

    ProductResponse addProduct(CreateProductRequest request);

    Product save(Product product);

    ProductResponse getById(long id);

    Product getByName(String name);
}
