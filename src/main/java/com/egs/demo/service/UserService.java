package com.egs.demo.service;

import com.egs.demo.model.dto.request.LoginRequest;
import com.egs.demo.model.dto.request.UserRegistrationRequest;
import com.egs.demo.model.dto.response.UserResponse;
import com.egs.demo.model.enums.UserState;

public interface UserService {

    UserResponse registerUser(UserRegistrationRequest registrationReq);

    int updateUserState(UserState state, Long id);

    void login(LoginRequest request);

    void delete(long userId);

}
