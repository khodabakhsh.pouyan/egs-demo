package com.egs.demo.service;

import com.egs.demo.model.dto.request.CreateCategoryRequest;
import com.egs.demo.model.dto.response.CategoryResponse;
import com.egs.demo.model.entity.Category;

public interface CategoryService {

    Category save(Category category);

    CategoryResponse addCategory(CreateCategoryRequest request);

    CategoryResponse getByName(String name);
}
