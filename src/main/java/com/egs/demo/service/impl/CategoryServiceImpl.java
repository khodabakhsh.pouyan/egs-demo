package com.egs.demo.service.impl;

import com.egs.demo.mapper.CategoryMapper;
import com.egs.demo.model.dto.request.CreateCategoryRequest;
import com.egs.demo.model.dto.response.CategoryResponse;
import com.egs.demo.model.entity.Category;
import com.egs.demo.model.repository.CategoryRepository;
import com.egs.demo.service.CategoryService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CategoryServiceImpl implements CategoryService {


    private final CategoryRepository categoryRepository;
    private final CategoryMapper categoryMapper;

    public CategoryServiceImpl(CategoryRepository categoryRepository, CategoryMapper categoryMapper) {
        this.categoryRepository = categoryRepository;
        this.categoryMapper = categoryMapper;
    }

    @Override
    @Transactional
    public Category save(Category category) {
        return categoryRepository.save(category);
    }

    @Override
    @Transactional
    public CategoryResponse addCategory(CreateCategoryRequest request) {
        Category category = new Category()
                .setName(request.getName());
        return categoryMapper.categoryToCategoryResponse(categoryRepository.save(category));
    }

    @Override
    public CategoryResponse getByName(String name) {
        return categoryMapper.categoryToCategoryResponse(categoryRepository.getByName(name));
    }
}
