package com.egs.demo.service.impl;

import com.egs.demo.mapper.ProductMapper;
import com.egs.demo.model.dto.request.CreateProductRequest;
import com.egs.demo.model.dto.request.RatingRequest;
import com.egs.demo.model.dto.response.ProductResponse;
import com.egs.demo.model.entity.Product;
import com.egs.demo.model.repository.CategoryRepository;
import com.egs.demo.model.repository.ProductRepository;
import com.egs.demo.service.ProductService;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;


@Service
public class ProductServiceImpl implements ProductService {

    private final ProductRepository productRepository;
    private final CategoryRepository categoryRepository;
    private final ProductMapper productMapper;

    public ProductServiceImpl(ProductRepository productRepository, CategoryRepository categoryRepository, ProductMapper productMapper) {
        this.productRepository = productRepository;
        this.categoryRepository = categoryRepository;
        this.productMapper = productMapper;
    }

    @Override
    public List<ProductResponse> findAll(Specification<Product> specification) {
        List<ProductResponse> productResponses = new ArrayList<>();
        for (Product product : productRepository.findAll(specification)) {
            productResponses.add(productMapper.productToProductResponse(product));
        }
        return productResponses;
    }

    @Override
    @Transactional
    public int rateProduct(RatingRequest ratingRequest) {
        if (ratingRequest.getRate() <= 5 && ratingRequest.getRate() >= 1)
            return productRepository.setProductRate(ratingRequest.getRate(), ratingRequest.getProductId());
        return 0;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRES_NEW)
    public void delete(Product product) {
        productRepository.delete(product);
    }

    @Override
    @Transactional
    public void deleteById(long productId) {
        productRepository.deleteById(productId);
    }

    @Override
    @Transactional
    public ProductResponse addProduct(CreateProductRequest request) {
        Product product = new Product()
                .setName(request.getName())
                .setRate(request.getRate())
                .setPrice(request.getPrice())
                .setCategory(categoryRepository.getById(request.getCategoryId()));
        return productMapper.productToProductResponse(productRepository.save(product));
    }

    @Override
    @Transactional
    public Product save(Product product) {
        return productRepository.save(product);
    }

    @Override
    public ProductResponse getById(long id) {
        return productMapper.productToProductResponse(productRepository.getById(id));
    }

    @Override
    public Product getByName(String name) {
        return productRepository.getByName(name);
    }

}
