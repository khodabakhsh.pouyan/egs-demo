package com.egs.demo.service.impl;

import com.egs.demo.mapper.UserMapper;
import com.egs.demo.model.dto.request.LoginRequest;
import com.egs.demo.model.dto.request.UserRegistrationRequest;
import com.egs.demo.model.dto.response.UserResponse;
import com.egs.demo.model.entity.Role;
import com.egs.demo.model.entity.Users;
import com.egs.demo.model.enums.UserState;
import com.egs.demo.model.repository.RoleRepository;
import com.egs.demo.model.repository.UserRepository;
import com.egs.demo.service.UserService;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Set;

@Service
public class UserServiceImpl implements UserService {

    private final BCryptPasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final RoleRepository roleRepository;
    private final UserMapper userMapper;
    private final UserDetailsService userDetailsService;

    public UserServiceImpl(BCryptPasswordEncoder passwordEncoder,
                           UserRepository userRepository,
                           RoleRepository roleRepository,
                           UserMapper userMapper,
                           UserDetailsService userDetailsService) {
        this.passwordEncoder = passwordEncoder;
        this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.userMapper = userMapper;
        this.userDetailsService = userDetailsService;
    }

    @Override
    @Transactional
    public UserResponse registerUser(UserRegistrationRequest registrationReq) {
        Users registeredUser = userRepository.findByUserName(registrationReq.getUsername());
        Role role = roleRepository.findByRoleName("ROLE_USER");
        if (registeredUser == null) {
            Users users = new Users()
                    .setFirstName(registrationReq.getFirstName())
                    .setLastName(registrationReq.getLastName())
                    .setUserName(registrationReq.getUsername())
                    .setPassword(passwordEncoder.encode(registrationReq.getPassword()))
                    .setRoles(Set.of(role))
                    .setState(UserState.UNBLOCKED);
            return userMapper.userToUserResponse(userRepository.saveAndFlush(users));
        }
        throw new IllegalArgumentException("User has already been registered");
    }

    @Override
    @Transactional
    public int updateUserState(UserState state, Long id) {
        return userRepository.updateState(state, id);
    }

    @Override
    public void login(LoginRequest request) {
        UserDetails principal = userDetailsService.loadUserByUsername(request.getUsername());
        if (passwordEncoder.matches(request.getPassword(), principal.getPassword())) {
            Authentication authentication = new UsernamePasswordAuthenticationToken(principal,
                    principal.getPassword(),
                    principal.getAuthorities());
            SecurityContext context = SecurityContextHolder.createEmptyContext();
            context.setAuthentication(authentication);
        }
    }

    @Override
    @Transactional
    public void delete(long userId) {
        userRepository.deleteById(userId);
    }
}
