package com.egs.demo.service.impl;

import com.egs.demo.mapper.CommentMapper;
import com.egs.demo.model.dto.request.CreateCommentRequest;
import com.egs.demo.model.dto.response.CommentResponse;
import com.egs.demo.model.entity.Comment;
import com.egs.demo.model.repository.CommentRepository;
import com.egs.demo.model.repository.ProductRepository;
import com.egs.demo.model.repository.UserRepository;
import com.egs.demo.service.CommentService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class CommentServiceImpl implements CommentService {

    private final CommentRepository commentRepository;
    private final ProductRepository productRepository;
    private final UserRepository userRepository;
    private final CommentMapper commentMapper;

    public CommentServiceImpl(CommentRepository commentRepository, ProductRepository productRepository, UserRepository userRepository, CommentMapper commentMapper) {
        this.commentRepository = commentRepository;
        this.productRepository = productRepository;
        this.userRepository = userRepository;
        this.commentMapper = commentMapper;
    }

    @Override
    @Transactional
    public CommentResponse addComment(CreateCommentRequest createCommentRequest) {
        Comment comment = new Comment()
                .setProduct(productRepository.getById(createCommentRequest.getProductId()))
                .setUser(userRepository.getById(createCommentRequest.getUserId()))
                .setText(createCommentRequest.getText());
        return commentMapper.commentToCommentResponse(commentRepository.save(comment));
    }

    @Override
    public List<CommentResponse> getProductCommentsByproductId(long productId) {
        return commentMapper.commentsToCommentResponses(commentRepository.getAllByProductId(productId));
    }
}
