package com.egs.demo.model.enums;

public enum RoleType {
    ADMIN,
    USER
}
