package com.egs.demo.model.enums;

public enum UserState {
    BLOCKED,
    UNBLOCKED
}
