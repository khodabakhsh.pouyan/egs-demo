package com.egs.demo.model.entity;

import com.egs.demo.model.enums.UserState;
import com.vladmihalcea.hibernate.type.array.EnumArrayType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.*;


@Entity
public class Users {
    private long id;
    private String userName;
    private String firstName;
    private String lastName;
    private String password;
    private UserState state;
    private Timestamp createDate;
    private Set<Role> roles;
    private List<Comment> comments;

    @Id
    @SequenceGenerator(name = "users_id_seq",
            sequenceName = "users_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "users_id_seq")
    public long getId() {
        return id;
    }

    public Users setId(long id) {
        this.id = id;
        return this;
    }

    public String getUserName() {
        return userName;
    }

    public Users setUserName(String userName) {
        this.userName = userName;
        return this;
    }

    public Users setPassword(String password) {
        this.password = password;
        return this;
    }

    public String getPassword() {
        return password;
    }
    @Enumerated(EnumType.STRING)
    public UserState getState() {
        return state;
    }

    public Users setState(UserState state) {
        this.state = state;
        return this;
    }

    @CreationTimestamp
    public Timestamp getCreateDate() {
        return createDate;
    }

    public Users setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
        return this;
    }

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "user_role",
            joinColumns = {@JoinColumn(name = "user_id")},
            inverseJoinColumns = {@JoinColumn(name = "role_id")})
    public Set<Role> getRoles() {
        return roles;
    }

    public Users setRoles(Set<Role> roles) {
        this.roles = roles;
        return this;
    }

    @OneToMany(mappedBy = "user")
    public List<Comment> getComments() {
        return comments;
    }

    public Users setComments(List<Comment> comments) {
        this.comments = comments;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public Users setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public Users setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Users users = (Users) o;
        return id == users.id && Objects.equals(userName, users.userName) && Objects.equals(firstName, users.firstName) && Objects.equals(lastName, users.lastName) && Objects.equals(password, users.password) && state == users.state && Objects.equals(createDate, users.createDate) && Objects.equals(roles, users.roles) && Objects.equals(comments, users.comments);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, userName, firstName, lastName, password, state, createDate, roles, comments);
    }
}
