package com.egs.demo.model.entity;

import com.egs.demo.model.enums.RoleType;
import com.egs.demo.model.enums.UserState;
import com.vladmihalcea.hibernate.type.array.EnumArrayType;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.TypeDef;
import org.hibernate.annotations.TypeDefs;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Arrays;
import java.util.Objects;

@Entity
public class Role {
    private long id;
    private String roleName;
    private Timestamp createDate;

    @Id
    @SequenceGenerator(name = "role_id_seq",
            sequenceName = "role_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "role_id_seq")
    public long getId() {
        return id;
    }

    public Role setId(long id) {
        this.id = id;
        return this;
    }

    public String getRoleName() {
        return roleName;
    }

    public Role setRoleName(String roleName) {
        this.roleName = roleName;
        return this;
    }

    @CreationTimestamp
    public Timestamp getCreateDate() {
        return createDate;
    }

    public Role setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Role role = (Role) o;
        return id == role.id && Objects.equals(roleName, role.roleName) && Objects.equals(createDate, role.createDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, roleName, createDate);
    }
}
