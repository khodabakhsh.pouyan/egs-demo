package com.egs.demo.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
public class Comment {
    private long id;
    private String text;
    private Users user;
    private Product product;
    private Timestamp createDate;

    @Id
    @SequenceGenerator(name = "comment_id_seq",
            sequenceName = "comment_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "comment_id_seq")
    public long getId() {
        return id;
    }

    public Comment setId(long id) {
        this.id = id;
        return this;
    }

    @Basic
    @Column(name = "text")
    public String getText() {
        return text;
    }

    public Comment setText(String text) {
        this.text = text;
        return this;
    }

    @Basic
    @Column(name = "create_date")
    @CreationTimestamp
    public Timestamp getCreateDate() {
        return createDate;
    }

    public Comment setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comment comment = (Comment) o;
        return id == comment.id && Objects.equals(text, comment.text) && Objects.equals(createDate, comment.createDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, text, createDate);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    public Users getUser() {
        return user;
    }

    public Comment setUser(Users user) {
        this.user = user;
        return this;
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id", referencedColumnName = "id")
    public Product getProduct() {
        return product;
    }

    public Comment setProduct(Product product) {
        this.product = product;
        return this;
    }
}
