package com.egs.demo.model.entity;

import org.hibernate.annotations.CreationTimestamp;

import javax.persistence.*;
import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "product")
public class Product {
    private long id;
    private String name;
    private BigDecimal price;
    private Integer rate;
    private Timestamp createDate;
    private Category category;
    private List<Comment> comments;

    @Id
    @SequenceGenerator(name = "product_id_seq",
            sequenceName = "product_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE,
            generator = "product_id_seq")
    public long getId() {
        return id;
    }

    public Product setId(long id) {
        this.id = id;
        return this;
    }

    @Column(name = "name")
    public String getName() {
        return name;
    }

    public Product setName(String name) {
        this.name = name;
        return this;
    }

    @Column(name = "price")
    public BigDecimal getPrice() {
        return price;
    }

    public Product setPrice(BigDecimal price) {
        this.price = price;
        return this;
    }

    @Column(name = "rate")
    public Integer getRate() {
        return rate;
    }

    public Product setRate(Integer rate) {
        this.rate = rate;
        return this;
    }

    @Column(name = "create_date")
    @CreationTimestamp
    public Timestamp getCreateDate() {
        return createDate;
    }

    public Product setCreateDate(Timestamp createDate) {
        this.createDate = createDate;
        return this;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Product product = (Product) o;
        return id == product.id && Objects.equals(name, product.name) && Objects.equals(price, product.price) && Objects.equals(rate, product.rate) && Objects.equals(createDate, product.createDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, price, rate, createDate);
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    public Category getCategory() {
        return category;
    }

    public Product setCategory(Category category) {
        this.category = category;
        return this;
    }

    @OneToMany(mappedBy = "product")
    public List<Comment> getComments() {
        return comments;
    }

    public Product setComments(List<Comment> comments) {
        this.comments = comments;
        return this;
    }
}
