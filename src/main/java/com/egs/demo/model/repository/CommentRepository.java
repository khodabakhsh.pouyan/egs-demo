package com.egs.demo.model.repository;

import com.egs.demo.model.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Long> {

    @Query("from Comment c " +
            "inner join Product p on c.product.id = p.id where p.id =:id")
    List<Comment> getAllByProductId(Long id);
}