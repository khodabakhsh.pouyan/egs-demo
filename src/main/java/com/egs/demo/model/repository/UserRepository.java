package com.egs.demo.model.repository;

import com.egs.demo.model.entity.Users;
import com.egs.demo.model.enums.UserState;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

public interface UserRepository extends JpaRepository<Users, Long> {

    Users findByUserName(String username);

    @Modifying
    @Query("update Users set state = :state where id = :id")
    int updateState(UserState state, Long id);

}