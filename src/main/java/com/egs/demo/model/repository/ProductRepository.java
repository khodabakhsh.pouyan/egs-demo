package com.egs.demo.model.repository;

import com.egs.demo.model.dto.request.RatingRequest;
import com.egs.demo.model.entity.Category;
import com.egs.demo.model.entity.Product;
import org.springframework.data.domain.Page;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface ProductRepository extends JpaRepository<Product, Long>, JpaSpecificationExecutor<Product> {

    @Modifying
    @Query("update Product p set p.rate = ?1 where p.id = ?2")
    int setProductRate(int rate, long productId);

    Product getByName(String name);

    @Query("from Product p join fetch p.category where p.id = :id")
    Product getById(long id);
}