package com.egs.demo.model.repository;

import com.egs.demo.model.entity.Category;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CategoryRepository extends JpaRepository<Category, Long> {

    Category getById(long id);

    Category getByName(String name);


}