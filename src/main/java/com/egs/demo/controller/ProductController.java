package com.egs.demo.controller;

import com.egs.demo.mapper.ProductMapper;
import com.egs.demo.model.dto.request.CreateCommentRequest;
import com.egs.demo.model.dto.request.CreateProductRequest;
import com.egs.demo.model.dto.request.RatingRequest;
import com.egs.demo.model.dto.response.CommentResponse;
import com.egs.demo.model.dto.response.ProductResponse;
import com.egs.demo.model.entity.Product;
import com.egs.demo.model.specifications.ProductSpecificationBuilder;
import com.egs.demo.service.CommentService;
import com.egs.demo.service.ProductService;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@RestController
@RequestMapping("/api/product")
@PreAuthorize("isAuthenticated()")
public class ProductController {

    private final ProductService productService;
    private final CommentService commentService;

    public ProductController(ProductService productService, CommentService commentService) {
        this.productService = productService;
        this.commentService = commentService;
    }

    @GetMapping("/search")
    public ResponseEntity<List<ProductResponse>> search(Pageable pageable, @RequestParam("search") String search) {
        ProductSpecificationBuilder builder = new ProductSpecificationBuilder();
        Pattern pattern = Pattern.compile("(\\w+?)([:<>])(\\w+?),");
        Matcher matcher = pattern.matcher(search + ",");
        while (matcher.find()) {
            builder.with(matcher.group(1), matcher.group(2), matcher.group(3));
        }

        Specification<Product> spec = builder.build();
        return ResponseEntity.ok(productService.findAll(spec));
    }

    @PutMapping("/rate")
    public ResponseEntity<String> rate(@RequestBody RatingRequest ratingRequest) {
        if (productService.rateProduct(ratingRequest) != 0)
            return ResponseEntity.ok("successfuly rated");
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    @PostMapping("/comment")
    public ResponseEntity<CommentResponse> addComment(@RequestBody CreateCommentRequest request) {
        return ResponseEntity.ok(commentService.addComment(request));
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<ProductResponse> create(@RequestBody CreateProductRequest request) {
        if (productService.getByName(request.getName()) == null)
            return ResponseEntity.ok(productService.addProduct(request));
        return ResponseEntity.status(HttpStatus.NOT_ACCEPTABLE).build();
    }

    @DeleteMapping("/{productId}")
    @PreAuthorize("hasRole('ROLE_ADMIN')")
    public ResponseEntity<String> delete(@PathVariable("productId") long productId) {
        productService.deleteById(productId);
        return ResponseEntity.ok("successfully deleted");
    }

    @GetMapping("/{productId}")
    public ResponseEntity<ProductResponse> get(@PathVariable("productId") long productId) {
        return ResponseEntity.ok(productService.getById(productId));
    }

    @GetMapping("/comments/{productId}")
    public ResponseEntity<List<CommentResponse>> getProductCommentsById(@PathVariable("productId") long productId) {
        return ResponseEntity.ok(commentService.getProductCommentsByproductId(productId));
    }
}
