package com.egs.demo.controller;

import com.egs.demo.model.dto.request.LoginRequest;
import com.egs.demo.model.dto.request.UserRegistrationRequest;
import com.egs.demo.model.dto.response.UserResponse;
import com.egs.demo.model.enums.UserState;
import com.egs.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/user")
public class UserManagementController {

    private final UserService userService;

    public UserManagementController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping("/sign-up")
    public ResponseEntity<UserResponse> signUp(@RequestBody UserRegistrationRequest request) {
        try {
            return ResponseEntity.ok(userService.registerUser(request));
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
        }
    }

    @PostMapping("/login")
    public ResponseEntity<String> login(@RequestBody LoginRequest request) {
        userService.login(request);
        return ResponseEntity.ok("Successful login");
    }

    @PutMapping("/{userId}/{state}")
    @PreAuthorize("hasAnyRole('ROLE_ADMIN')")
    public ResponseEntity<Integer> changeState(@PathVariable("state") UserState state,
                                                    @PathVariable("userId") long userId) {
        return ResponseEntity.ok(userService.updateUserState(state, userId));
    }

}
