package com.egs.demo.mapper;

import com.egs.demo.model.dto.response.ProductResponse;
import com.egs.demo.model.entity.Product;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface ProductMapper {

    ProductMapper INSTANCE = Mappers.getMapper(ProductMapper.class);

    @Mapping(source = "product.category.id", target = "categoryId")
    ProductResponse productToProductResponse(Product product);
}
