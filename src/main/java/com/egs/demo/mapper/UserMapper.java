package com.egs.demo.mapper;

import com.egs.demo.model.dto.response.UserResponse;
import com.egs.demo.model.entity.Users;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    @Mapping(source = "userName" , target = "username")
    UserResponse userToUserResponse(Users users);
}
