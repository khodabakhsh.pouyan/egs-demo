package com.egs.demo.service.impl;

import com.egs.demo.model.dto.request.UserRegistrationRequest;
import com.egs.demo.model.dto.response.UserResponse;
import com.egs.demo.model.entity.Users;
import com.egs.demo.model.enums.UserState;
import com.egs.demo.service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class UsersServiceImplTest {

    private UserResponse user;

    @BeforeEach
    void setUp() {
        var userDTO = UserRegistrationRequest.builder()
                .firstName("pouyan")
                .lastName("khodabakhsh")
                .username("pouyan021")
                .password("123456")
                .build();
        user = userService.registerUser(userDTO);
    }

    @AfterEach
    void tearDown() {
        if (user != null) {
            userService.delete(user.getId());
        }
    }

    @Autowired
    private UserService userService;

    @Test
    void register() {
        var userDTO = UserRegistrationRequest.builder()
                .firstName("pouyan")
                .lastName("khodabakhsh")
                .username("pouyan021")
                .password("123456")
                .build();

        assertNotNull(userService.registerUser(userDTO));
    }

}