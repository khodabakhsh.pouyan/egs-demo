package com.egs.demo.service.impl;

import com.egs.demo.model.dto.request.CreateCategoryRequest;
import com.egs.demo.model.dto.request.CreateCommentRequest;
import com.egs.demo.model.dto.request.CreateProductRequest;
import com.egs.demo.model.dto.request.UserRegistrationRequest;
import com.egs.demo.model.dto.response.UserResponse;
import com.egs.demo.model.entity.Category;
import com.egs.demo.model.entity.Product;
import com.egs.demo.model.repository.CategoryRepository;
import com.egs.demo.model.repository.ProductRepository;
import com.egs.demo.service.CommentService;
import com.egs.demo.service.UserService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;
import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class CommentServiceImplTest {

    @Autowired
    private UserService userService;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CommentService commentService;

    private UserResponse user;

    private Category category;

    private Product product;



    @BeforeEach
    void setUp() {
        var userDTO = UserRegistrationRequest.builder()
                .firstName("pouyan")
                .lastName("khodabakhsh")
                .username("pouyan021")
                .password("123456")
                .build();
        user = userService.registerUser(userDTO);

        var createCategoryReq = CreateCategoryRequest.builder()
                .name("test")
                .build();
        category = new Category()
                .setName(createCategoryReq.getName());
        category = categoryRepository.save(category);

        var createProductRequest = CreateProductRequest.builder()
                .name("test")
                .price(BigDecimal.valueOf(10))
                .rate(3)
                .categoryId(category.getId())
                .build();

        product = new Product()
                .setCategory(category)
                .setName(createCategoryReq.getName())
                .setPrice(createProductRequest.getPrice())
                .setRate(createProductRequest.getRate());
        product = productRepository.save(product);
    }

    @AfterEach
    void tearDown() {

    }

    @Test
    void addComment() {
        var createComment = CreateCommentRequest.builder()
                .userId(user.getId())
                .productId(product.getId())
                .text("test")
                .build();
        assertNotNull(commentService.addComment(createComment));
    }
}