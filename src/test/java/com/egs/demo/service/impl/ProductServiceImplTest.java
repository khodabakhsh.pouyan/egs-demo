package com.egs.demo.service.impl;

import com.egs.demo.model.dto.request.CreateCategoryRequest;
import com.egs.demo.model.dto.request.CreateProductRequest;
import com.egs.demo.model.dto.request.RatingRequest;
import com.egs.demo.model.dto.response.ProductResponse;
import com.egs.demo.model.entity.Category;
import com.egs.demo.model.entity.Product;
import com.egs.demo.model.repository.CategoryRepository;
import com.egs.demo.model.repository.ProductRepository;
import com.egs.demo.service.CategoryService;
import com.egs.demo.service.ProductService;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.math.BigDecimal;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
class ProductServiceImplTest {

    @Autowired
    private ProductService productService;

    @Autowired
    private CategoryService categoryService;


    private Product product;
    private Category category;

    @BeforeEach
    void setUp() {

        var createCategoryReq = CreateCategoryRequest.builder()
                .name("test")
                .build();
        category = new Category()
                .setName(createCategoryReq.getName());
        category = categoryService.save(category);

        var createProductReq = CreateProductRequest.builder()
                .name("test")
                .price(BigDecimal.valueOf(10))
                .rate(2)
                .categoryId(category.getId())
                .build();
        product = new Product()
                .setCategory(category)
                .setName(createCategoryReq.getName())
                .setPrice(createProductReq.getPrice())
                .setRate(createProductReq.getRate());

        product = productService.save(product);
    }

    @AfterEach
    void tearDown() {
        if (product != null) {
            productService.delete(product);
        }
    }

    @Test
    void findAll() {
    }

    @Test
    void rateProduct() {
        var ratingRequest = RatingRequest.builder()
                .productId(product.getId())
                .rate(product.getRate())
                .build();
        assertNotSame(productService.rateProduct(ratingRequest), product.getId());
    }

    @Test
    void addProduct() {
        var createProductReq = CreateProductRequest.builder()
                .name("test")
                .price(BigDecimal.valueOf(10))
                .rate(2)
                .categoryId(category.getId())
                .build();
        ProductResponse productResponse = productService.addProduct(createProductReq);
        product = productService.getById(productResponse.getId());
        assertNotNull(product);
    }
}